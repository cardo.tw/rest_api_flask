from flask import Flask

from flask.views import MethodView

class RestAPI(MethodView):

    def get(self):
        return "I'm a get"

    def post(self):
        return "I'm a post"

    def put(self):
        return "I'm a update"

    def delete(self):
        return "I'm a delete"

app = Flask(__name__)

app.add_url_rule('/resource/', view_func=RestAPI.as_view('resources'), methods=['GET', 'PUT', 'DELETE', 'POST'])
